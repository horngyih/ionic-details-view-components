import { Component } from '@angular/core';
import { TimeBasedDetailsViewComponent } from '../time-based-details-view/time-based-details-view';
import { TimeBasedDetail } from '../../models/detail/timebased-detail';

/**
 * Generated class for the PromotionDetailsViewComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'promotion-details-view',
  templateUrl: 'promotion-details-view.html'
})
export class PromotionDetailsViewComponent extends TimeBasedDetailsViewComponent {
  constructor() {
    super();
    this.detail = new TimeBasedDetail( "Promotion", "Promotion Description", "http://placehold.it/300x300?text=Promotion Image", new Date(), new Date() );
  }

}
