import { Component } from '@angular/core';

import { Detail } from '../../models/detail/detail';
import { RoomDetail } from '../../models/detail/room-detail';

/**
 * Generated class for the DetailsViewComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'details-view',
  templateUrl: 'details-view.html'
})
export class DetailsViewComponent {

  detail: Detail;

  constructor() {
    this.detail = new Detail( "Details", "Detailed description", "http://placehold.it/300x300?text=Details Image" );
  }

}
