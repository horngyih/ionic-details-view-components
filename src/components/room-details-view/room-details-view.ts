import { Component } from '@angular/core';
import { DetailsViewComponent } from '../details-view/details-view';
import { RoomDetail } from '../../models/detail/room-detail';

/**
 * Generated class for the RoomDetailsViewComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'room-details-view',
  templateUrl: 'room-details-view.html'
})
export class RoomDetailsViewComponent
  extends DetailsViewComponent {
  constructor() {
    super();
    this.detail = new RoomDetail( "Room Name", "Room Description", "http://placehold.it/300x300?text=Room Image", 3 );
  }
}
