import { Component } from '@angular/core';
import { TimeBasedDetailsViewComponent } from '../time-based-details-view/time-based-details-view';
import { EventDetail } from '../../models/detail/event-detail';

/**
 * Generated class for the EventDetailsViewComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'event-details-view',
  templateUrl: 'event-details-view.html'
})
export class EventDetailsViewComponent 
  extends TimeBasedDetailsViewComponent{

    venue: String;

  constructor() {
    super();
    this.detail = new EventDetail( "Event Detail", "Event Description", "http://placehold.it/300x300?text=Event Image", new Date(), new Date(), "Event Venue" );
  }

}
