import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { DetailsViewComponent } from './details-view/details-view';
import { IonicModule } from 'ionic-angular';
import { RoomDetailsViewComponent } from './room-details-view/room-details-view';
import { TimeBasedDetailsViewComponent } from './time-based-details-view/time-based-details-view';
import { EventDetailsViewComponent } from './event-details-view/event-details-view';
import { PromotionDetailsViewComponent } from './promotion-details-view/promotion-details-view';

@NgModule({
	declarations: [DetailsViewComponent,
    RoomDetailsViewComponent,
    TimeBasedDetailsViewComponent,
    EventDetailsViewComponent,
    PromotionDetailsViewComponent],
	imports: [ IonicModule ],
	exports: [DetailsViewComponent,
    RoomDetailsViewComponent,
    TimeBasedDetailsViewComponent,
    EventDetailsViewComponent,
    PromotionDetailsViewComponent],
	schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ComponentsModule {}
