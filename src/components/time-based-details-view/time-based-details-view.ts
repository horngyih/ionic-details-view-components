import { Component } from '@angular/core';
import { DetailsViewComponent } from '../details-view/details-view';
import { TimeBasedDetail } from '../../models/detail/timebased-detail';

/**
 * Generated class for the TimeBaseDetailsViewComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'time-based-details-view',
  templateUrl: 'time-based-details-view.html'
})
export class TimeBasedDetailsViewComponent 
  extends DetailsViewComponent{
  constructor() {
    super();
    this.detail = new TimeBasedDetail( "Detail", "Description", "http://placehold.it/300x300?text=TimeBased", new Date(), new Date() );
  }

}
