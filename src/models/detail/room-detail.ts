import { Detail } from "./detail";

export class RoomDetail extends Detail{
    constructor( title: String, description: String, imgURL: String, public roomCapacity: Number){
        super( title, description, imgURL );
    }
}