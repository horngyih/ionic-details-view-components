import { Detail } from "./detail";

export class TimeBasedDetail extends Detail{
    constructor( title: String, description: String, imgURL: String,public startDate: Date,public endDate: Date ){
        super( title, description, imgURL );
    }
}