import { TimeBasedDetail } from "./timebased-detail";

export class EventDetail extends TimeBasedDetail{
    constructor( title: String, description: String, imgURL : String, startDate: Date, endDate: Date, public venue: String){
        super( title, description, imgURL, startDate, endDate );
    }
}